package EX1Test;

import EX1.ConnectPoor;
import EX1.StudentBean;
import com.alibaba.fastjson.JSON;
import org.junit.Test;

import java.sql.Connection;
import java.util.ArrayList;


public class TestStudent {
    StudentBean s = new StudentBean("张三",20);
    String jStudent;

//    对象转换
    @Test
    public void Stu2JSON(){
//        将Java对象转为json字符串
        jStudent = JSON.toJSONString(s);
        System.out.println("json: "+jStudent);
//        将json字符串转化为Java对象
        System.out.println("oject: "+JSON.parseObject(jStudent, StudentBean.class));

    }


}
