package EX1;

import EX1.ConnectPoor;
import com.alibaba.fastjson.JSON;

import java.sql.*;
import java.lang.reflect.*;

//7. 用反射机制设计一个能向数据库中插入任意表记录的方法
//8. 用git将该项目提交到Gitee上

public class ReflexInsert<T>{

    //将bean的所有属性存入数据库
    public void insert(T c){
        try {
            Field[] fields = c.getClass().getDeclaredFields();
            int count = fields.length;

            ConnectPoor poor = new ConnectPoor();
            Connection connection = poor.getCon();
//            查看类名，查询数据库是否存在bean的表

            String c_name = c.getClass().getSimpleName();
            String select_table_name = "SELECT * from information_schema.`TABLES` WHERE TABLE_name= \'"+c_name+"\' and TABLE_schema='housework';";
            Statement statement = connection.createStatement();

            //          如果不存在类
            if(statement.execute(select_table_name)){
                //            创建表
                String cs = "";
                for (Field field : fields) {
                    field.setAccessible(true);
                    cs  += "`"+field.getName() +"`  json,"; //都用json类型存储
                }
                cs= cs.substring(0,cs.length()-1);

                String creat_table = "CREATE TABLE IF NOT EXISTS `"+c_name+"`(" +
                        "   `id` INT UNSIGNED  AUTO_INCREMENT primary key," +
                        cs+ ")ENGINE=InnoDB DEFAULT CHARSET=utf8;";

                int cbr = statement.executeUpdate(creat_table);
            }
            statement.close();

            StringBuffer sql = new StringBuffer(256);
            StringBuffer name = new StringBuffer(256);
            StringBuffer value = new StringBuffer(256);


            for (Field field : fields) {
                field.setAccessible(true);
                String n = field.getName();
                name.append(n);
                value.append('?');
                name.append(',');
                value.append(',');
            }


            name = name.delete(name.length()-1,name.length());
            value = value.delete(value.length()-1,value.length());

            String s = "INSERT INTO "+c.getClass().getSimpleName() +"( id, "+ name +")" + "VALUES ( null," +value +");";

            PreparedStatement ps = connection.prepareStatement(s);
            for(int i=1;i<=count;i++){
                fields[i-1].setAccessible(true);
                ps.setObject(i, JSON.toJSONString(fields[i-1].get(c)));
            }

            int rs = ps.executeUpdate();
            ps.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }

    }
}
