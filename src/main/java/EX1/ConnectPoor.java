package EX1;

import com.mysql.cj.jdbc.Driver;

import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedList;

//    6. 用单例模式设计一个获取数据库连接的类，要求
//    (1) 首先初始化N个数据库接，存入一个数据集中
//    (2) 需要数据库连接时，从该数据集中取出一个
//    (3) 使用完连接后，归还到数据集
public class ConnectPoor {
    private static int poorSize=8;
    private static boolean isrun=false;
    private static LinkedList<Connection> poor ;

    public ConnectPoor(){
        init();
    };

//    初始化连接池
    private static void init(){
        if(!isrun){
            isrun = true;
            poor = new LinkedList<Connection>();
            for(int i=0;i<poorSize;i++){
                try {
                    Class.forName("com.mysql.cj.jdbc.Driver");
                    Connection conn = DriverManager.getConnection(
                            "jdbc:mysql://127.0.0.1:3306/housework?useUnicode=true&characterEncoding=utf-8&useSSL=false&serverTimezone=UTC",
                            "root",
                            "mysql"
                    );
                    poor.add(conn);
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                } catch (ClassNotFoundException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

//   获取连接
    public static synchronized Connection getCon() {
        if(!poor.isEmpty()&&isrun){
            final Connection con = poor.poll();
            return con;
        }else {
            return null;
        }
    }
//归还连接
    public static void backCon(Connection con) throws SQLException {
        if(isrun){
            poor.add(con);
        }else{
            con.close();
        }
    }
//关闭连接池
    public static void close() throws SQLException {
        isrun = false;
        while (!poor.isEmpty()){
            poor.poll().close();
        }
    }
}
