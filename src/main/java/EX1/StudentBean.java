package EX1;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StudentBean {
    String name;
    int age=0;

    StudentBean(){
    }
    public StudentBean(String name, int age){
        this.name = name;
        this.age = age;
    }
}
